/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {},
    colors: {
      dark1: '#050505',
      dark2: '#1F1F1F',
      dark3: '#2D2D2D',
      dark4: '#3A3A3A',
      grey1: '#757575',
      grey2: '#E9E9E9',
      grey3: '#F4F4F4',
      white: '#FFFFFF',
      purple: '#A445ED',
      red: '#FF5252',
    },
    fontFamily: {
      inconsolata: ['Inconsolata', 'monospace'],
      inter: ['Inter', 'sans-serif'],
      lora: ['Lora', 'serif'],
    },
  },
  plugins: [],
}
