import Header from './components/Header'
import SearchBar from './components/SearchBar'
import Word from './components/Word'
import { useGlobalContext } from './context'

const App = () => {
  const { font } = useGlobalContext()
  return (
    <main
      className={`dark:bg-dark1 bg-grey3 ${
        font === 'Sans Serif'
          ? 'font-inter'
          : font === 'Serif'
          ? 'font-lora'
          : 'font-inconsolata'
      }`}
    >
      <Header />
      <SearchBar />
      <Word />
    </main>
  )
}
export default App
