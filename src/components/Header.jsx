import React, { useState } from 'react'
import logo from '../assets/images/logo.svg'
import arrow from '../assets/images/icon-arrow-down.svg'
import moon from '../assets/images/icon-moon.svg'
import Switch from './Switch'
import { useGlobalContext } from '../context'

const Header = () => {
  const [isFontMenuOpen, setIsFontMenuOpen] = useState(false)
  const { font, setFont } = useGlobalContext()

  const toggleFontMenu = () => {
    setIsFontMenuOpen(!isFontMenuOpen)
  }

  const selectFont = (font) => {
    setFont(font)
    setIsFontMenuOpen(false)
  }

  const fontOptions = ['Sans Serif', 'Serif', 'Mono']

  return (
    <header className="p-4 max-w-2xl mx-auto">
      <div className="flex justify-between items-center">
        <img src={logo} alt="logo" />
        <div className="flex items-center gap-x-5">
          <div className="relative">
            <div
              className="flex gap-x-2 cursor-pointer"
              onClick={toggleFontMenu}
            >
              <p className="font-bold dark:text-white">{font}</p>
              <img src={arrow} alt="arrow" />
            </div>
            {isFontMenuOpen && (
              <div className="absolute top-6 right-0 w-36 p-4 rounded-lg bg-white dark:bg-dark3 shadow-xl z-10">
                {fontOptions.map((fontOption, index) => (
                  <p
                    key={index}
                    className={`text-dark3 font-bold dark:text-white cursor-pointer ${
                      fontOption === 'Sans Serif'
                        ? 'font-inter'
                        : fontOption === 'Serif'
                        ? 'font-lora'
                        : 'font-inconsolata'
                    }`}
                    onClick={() => selectFont(fontOption)}
                  >
                    {fontOption}
                  </p>
                ))}
              </div>
            )}
          </div>

          <div className="separator-y"></div>
          <Switch />
          <img src={moon} alt="moon" />
        </div>
      </div>
    </header>
  )
}

export default Header
