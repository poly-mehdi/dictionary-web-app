import { useGlobalContext } from '../context'

const Switch = () => {
  const { isDarkTheme, toggleDarkTheme } = useGlobalContext()

  return (
    <div className={`theme-switcher dark:bg-purple`} onClick={toggleDarkTheme}>
      <div className={`switch ${isDarkTheme ? 'active-2' : ''}`}></div>
    </div>
  )
}
export default Switch
