import React, { useState } from 'react'
import iconSearch from '../assets/images/icon-search.svg'
import { useGlobalContext } from '../context'

const SearchBar = () => {
  const { setSearchTerm } = useGlobalContext()
  const [errorMessage, setErrorMessage] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()
    const searchValue = e.target.elements[0].value
    if (!searchValue) {
      setErrorMessage("Whoops, can't be empty…")
      return
    } else {
      setErrorMessage('')
      setSearchTerm(searchValue)
    }
  }

  return (
    <form className="px-4 py-1 max-w-2xl mx-auto" onSubmit={handleSubmit}>
      <div className="w-full flex relative items-center">
        <input
          type="text"
          placeholder="Search for any word..."
          className="w-full bg-grey2 rounded-xl py-2 pl-5 pr-8 text-lg outline-none dark:bg-dark2 dark:text-white"
        />
        <button className="absolute right-2" type="submit">
          <img src={iconSearch} alt="Submit" />
        </button>
      </div>
      {errorMessage && <p className="text-red">{errorMessage}</p>}
    </form>
  )
}

export default SearchBar
