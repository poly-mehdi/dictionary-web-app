import newWindow from '../assets/images/icon-new-window.svg'
import iconPlay from '../assets/images/icon-play.svg'
import { useGlobalContext } from '../context'
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'

const url = 'https://api.dictionaryapi.dev/api/v2/entries/en/'

const Word = () => {
  const { searchTerm } = useGlobalContext()
  const response = useQuery({
    queryKey: ['term', searchTerm],
    queryFn: async () => {
      const result = await axios.get(`${url}${searchTerm}`)
      return result.data
    },
    retry: 1,
  })
  const playSound = () => {
    const audio = document.getElementById('myAudio')
    audio.play()
  }

  if (response.isLoading) {
    return (
      <section className="image-container text-center mt-6 text-2xl font-bold mt-28 max-w-2xl mx-auto h-screen">
        <h4 className="dark:text-white">Loading...</h4>
      </section>
    )
  }
  if (response.isError) {
    return (
      <section className="image-container text-center text-2xl font-bold mt-28 flex flex-col items-center max-w-2xl mx-auto h-screen">
        <span style={{ fontSize: '48px' }} className="mb-12">
          😕
        </span>
        <h4 className="mb-6 text-dark3 text-xl dark:text-white">
          No Definitions Found
        </h4>
        <p className="text-grey1 text-sm font-light">
          Sorry pal, we couldn't find definitions for the word you were looking
          for. You can try the search again at later time or head to the web
          instead.
        </p>
      </section>
    )
  }
  const { word, phonetic, phonetics, sourceUrls, meanings } = response.data[0]
  return (
    <div className="max-w-2xl mx-auto">
      <section className="p-4 flex justify-between items-center">
        <div className="flex flex-col gap-y-2">
          <p className="text-4xl font-semibold dark:text-white">{word}</p>
          <p className="text-purple text-lg tracking-wide">{phonetic}</p>
        </div>
        <img
          src={iconPlay}
          alt="icon-play"
          className="h-12 w-12 cursor-pointer"
          onClick={playSound}
        />
        <audio id="myAudio" type="audio/mp3" src={phonetics[0].audio} />
      </section>
      <section className="p-4">
        <div className="flex items-center gap-x-3 mb-6">
          <p className="text-xl italic font-semibold tracking-wide dark:text-white">
            noun
          </p>
          <div className="separator-x"></div>
        </div>
        <div>
          <p className="capitalize tracking-wide text-xl text-grey1 font-light mb-4">
            Meaning
          </p>
          <ul>
            {meanings[0].definitions.map((meaning, index) => {
              return (
                <li className="flex gap-x-4 mb-3" key={index}>
                  <div className="bullet mt-2"></div>
                  <p className="dark:text-white">{meaning.definition}</p>
                </li>
              )
            })}
          </ul>
          <div className="flex flex-row items-center gap-x-6  mb-4  py-4 overflow-hidden overflow-x-scroll">
            <p className="capitalize tracking-wide text-xl text-grey1 font-light">
              Synonyms
            </p>
            {meanings[0].synonyms.map((synonym, index) => {
              return (
                <p className="text-purple font-bold" key={index}>
                  {synonym}
                </p>
              )
            })}
          </div>
        </div>
        {meanings.length > 1 ? (
          <>
            <div className="flex items-center gap-x-3 mb-6">
              <p className="text-xl italic font-semibold tracking-wide dark:text-white">
                verb
              </p>
              <div className="separator-x"></div>
            </div>
            <div>
              <p className="capitalize tracking-wide text-xl text-grey1 font-light mb-4">
                Meaning
              </p>
              <ul>
                {meanings[1].definitions.map((meaning, index) => {
                  return (
                    <li className="flex gap-x-4 mb-3" key={index}>
                      <div className="bullet mt-2"></div>
                      <p className="dark:dark:text-white">
                        {meaning.definition}
                      </p>
                    </li>
                  )
                })}
              </ul>
              <div className="pl-7 text-grey1">
                <p>{meanings[1].definitions[0].example}</p>
              </div>
            </div>
          </>
        ) : null}

        <div className="separator-x mt-4"></div>
        <div className="mt-2 text-grey1">
          <p className="underline">Source</p>
          <div className="flex items-center gap-x-2">
            <p className="underline text-dark3 dark:text-white">
              {sourceUrls[0]}
            </p>
            <a href={sourceUrls[0]}>
              <img src={newWindow} alt="newWindow" />
            </a>
          </div>
        </div>
      </section>
    </div>
  )
}
export default Word
